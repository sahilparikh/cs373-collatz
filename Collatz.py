#!/usr/bin/env python3
"""
Solves the Collatz problem posed in HackerRank.
"""
# ----------
# Collatz.py
# ----------

from typing import IO, List

# ------------
# collatz_read
# ------------


def create_cache():
    """
    creates a meta cache of the max cycle lengths for ranges in multiples of 1000
    """
    max_cycle_length_cache = {
        (947001, 948000): 383,
        (954001, 955000): 383,
        (992001, 993000): 352,
        (699001, 700000): 367,
        (892001, 893000): 295,
        (336001, 337000): 366,
        (776001, 777000): 331,
        (173001, 174000): 347,
        (35001, 36000): 324,
        (604001, 605000): 372,
        (831001, 832000): 419,
        (588001, 589000): 315,
        (726001, 727000): 349,
        (765001, 766000): 331,
        (951001, 952000): 383,
        (843001, 844000): 331,
        (886001, 887000): 445,
        (761001, 762000): 362,
        (314001, 315000): 278,
        (552001, 553000): 315,
        (919001, 920000): 432,
        (20001, 21000): 256,
        (736001, 737000): 318,
        (337001, 338000): 366,
        (792001, 793000): 437,
        (522001, 523000): 333,
        (487001, 488000): 382,
        (475001, 476000): 320,
        (434001, 435000): 356,
        (409001, 410000): 312,
        (158001, 159000): 321,
        (632001, 633000): 385,
        (711001, 712000): 380,
        (198001, 199000): 329,
        (43001, 44000): 270,
        (381001, 382000): 374,
        (362001, 363000): 361,
        (305001, 306000): 265,
        (12001, 13000): 263,
        (342001, 343000): 353,
        (549001, 550000): 315,
        (145001, 146000): 295,
        (113001, 114000): 292,
        (979001, 980000): 334,
        (39001, 40000): 306,
        (802001, 803000): 388,
        (494001, 495000): 351,
        (640001, 641000): 416,
        (656001, 657000): 429,
        (243001, 244000): 337,
        (213001, 214000): 373,
        (136001, 137000): 313,
        (366001, 367000): 348,
        (311001, 312000): 309,
        (440001, 441000): 325,
        (2001, 3000): 217,
        (969001, 970000): 352,
        (866001, 867000): 326,
        (885001, 886000): 370,
        (406001, 407000): 361,
        (244001, 245000): 275,
        (537001, 538000): 377,
        (172001, 173000): 303,
        (479001, 480000): 351,
        (953001, 954000): 414,
        (200001, 201000): 342,
        (165001, 166000): 321,
        (599001, 600000): 372,
        (382001, 383000): 361,
        (774001, 775000): 331,
        (183001, 184000): 254,
        (595001, 596000): 328,
        (271001, 272000): 283,
        (122001, 123000): 318,
        (601001, 602000): 403,
        (76001, 77000): 307,
        (70001, 71000): 312,
        (74001, 75000): 325,
        (343001, 344000): 335,
        (275001, 276000): 345,
        (534001, 535000): 346,
        (687001, 688000): 380,
        (33001, 34000): 267,
        (55001, 56000): 260,
        (447001, 448000): 325,
        (650001, 651000): 341,
        (839001, 840000): 388,
        (124001, 125000): 287,
        (134001, 135000): 300,
        (215001, 216000): 280,
        (893001, 894000): 326,
        (185001, 186000): 329,
        (140001, 141000): 344,
        (554001, 555000): 421,
        (209001, 210000): 311,
        (175001, 176000): 272,
        (510001, 511000): 333,
        (744001, 745000): 318,
        (368001, 369000): 317,
        (129001, 130000): 344,
        (810001, 811000): 406,
        (91001, 92000): 333,
        (318001, 319000): 353,
        (478001, 479000): 351,
        (944001, 945000): 383,
        (518001, 519000): 439,
        (237001, 238000): 350,
        (578001, 579000): 346,
        (755001, 756000): 362,
        (598001, 599000): 341,
        (184001, 185000): 316,
        (556001, 557000): 346,
        (916001, 917000): 339,
        (676001, 677000): 336,
        (515001, 516000): 395,
        (531001, 532000): 346,
        (749001, 750000): 393,
        (180001, 181000): 365,
        (418001, 419000): 343,
        (896001, 897000): 370,
        (216001, 217000): 386,
        (654001, 655000): 323,
        (970001, 971000): 458,
        (525001, 526000): 408,
        (949001, 950000): 352,
        (547001, 548000): 377,
        (92001, 93000): 315,
        (245001, 246000): 319,
        (501, 1000): 179,
        (793001, 794000): 375,
        (391001, 392000): 330,
        (519001, 520000): 364,
        (987001, 988000): 396,
        (971001, 972000): 352,
        (789001, 790000): 406,
        (347001, 348000): 322,
        (965001, 966000): 321,
        (826001, 827000): 313,
        (841001, 842000): 313,
        (372001, 373000): 317,
        (163001, 164000): 321,
        (673001, 674000): 336,
        (994001, 995000): 365,
        (734001, 735000): 349,
        (388001, 389000): 330,
        (87001, 88000): 333,
        (169001, 170000): 334,
        (880001, 881000): 326,
        (495001, 496000): 320,
        (323001, 324000): 322,
        (29001, 30000): 272,
        (920001, 921000): 339,
        (10001, 11000): 268,
        (833001, 834000): 357,
        (748001, 749000): 318,
        (119001, 120000): 305,
        (156001, 157000): 383,
        (680001, 681000): 336,
        (287001, 288000): 327,
        (658001, 659000): 398,
        (480001, 481000): 413,
        (791001, 792000): 344,
        (652001, 653000): 354,
        (705001, 706000): 380,
        (411001, 412000): 330,
        (606001, 607000): 328,
        (319001, 320000): 322,
        (488001, 489000): 382,
        (511001, 512000): 470,
        (100001, 101000): 341,
        (715001, 716000): 305,
        (590001, 591000): 359,
        (550001, 551000): 346,
        (306001, 307000): 309,
        (423001, 424000): 418,
        (566001, 567000): 359,
        (923001, 924000): 308,
        (154001, 155000): 352,
        (967001, 968000): 308,
        (597001, 598000): 372,
        (574001, 575000): 328,
        (320001, 321000): 371,
        (103001, 104000): 248,
        (201001, 202000): 311,
        (435001, 436000): 281,
        (354001, 355000): 348,
        (958001, 959000): 321,
        (48001, 49000): 296,
        (281001, 282000): 358,
        (538001, 539000): 346,
        (13001, 14000): 276,
        (617001, 618000): 372,
        (573001, 574000): 359,
        (491001, 492000): 320,
        (712001, 713000): 411,
        (350001, 351000): 304,
        (766001, 767000): 331,
        (695001, 696000): 336,
        (624001, 625000): 385,
        (265001, 266000): 332,
        (934001, 935000): 414,
        (856001, 857000): 375,
        (889001, 890000): 370,
        (529001, 530000): 333,
        (508001, 509000): 377,
        (351001, 352000): 379,
        (445001, 446000): 338,
        (952001, 953000): 414,
        (868001, 869000): 357,
        (65001, 66000): 268,
        (383001, 384000): 330,
        (961001, 962000): 352,
        (485001, 486000): 338,
        (436001, 437000): 325,
        (874001, 875000): 388,
        (438001, 439000): 400,
        (141001, 142000): 326,
        (636001, 637000): 310,
        (646001, 647000): 323,
        (299001, 300000): 327,
        (317001, 318000): 309,
        (569001, 570000): 359,
        (628001, 629000): 279,
        (143001, 144000): 282,
        (836001, 837000): 344,
        (301001, 302000): 296,
        (505001, 506000): 364,
        (678001, 679000): 380,
        (262001, 263000): 301,
        (400001, 401000): 374,
        (681001, 682000): 336,
        (358001, 359000): 379,
        (796001, 797000): 468,
        (293001, 294000): 358,
        (210001, 211000): 342,
        (647001, 648000): 341,
        (24001, 25000): 264,
        (63001, 64000): 330,
        (931001, 932000): 308,
        (496001, 497000): 320,
        (236001, 237000): 337,
        (258001, 259000): 301,
        (907001, 908000): 326,
        (231001, 232000): 350,
        (662001, 663000): 292,
        (452001, 453000): 294,
        (202001, 203000): 342,
        (51001, 52000): 309,
        (516001, 517000): 302,
        (637001, 638000): 354,
        (313001, 314000): 340,
        (888001, 889000): 295,
        (389001, 390000): 436,
        (365001, 366000): 361,
        (360001, 361000): 410,
        (253001, 254000): 332,
        (721001, 722000): 367,
        (459001, 460000): 307,
        (40001, 41000): 288,
        (635001, 636000): 416,
        (326001, 327000): 247,
        (900001, 901000): 401,
        (227001, 228000): 306,
        (762001, 763000): 331,
        (269001, 270000): 345,
        (883001, 884000): 370,
        (8001, 9000): 247,
        (476001, 477000): 307,
        (267001, 268000): 345,
        (111001, 112000): 323,
        (126001, 127000): 331,
        (357001, 358000): 304,
        (108001, 109000): 279,
        (767001, 768000): 468,
        (769001, 770000): 393,
        (941001, 942000): 339,
        (309001, 310000): 340,
        (882001, 883000): 313,
        (437001, 438000): 387,
        (278001, 279000): 345,
        (430001, 431000): 312,
        (666001, 667000): 323,
        (199001, 200000): 329,
        (738001, 739000): 424,
        (750001, 751000): 362,
        (668001, 669000): 323,
        (768001, 769000): 331,
        (17001, 18000): 279,
        (60001, 61000): 335,
        (191001, 192000): 329,
        (752001, 753000): 318,
        (897001, 898000): 339,
        (564001, 565000): 421,
        (107001, 108000): 292,
        (983001, 984000): 383,
        (49001, 50000): 296,
        (732001, 733000): 380,
        (933001, 934000): 370,
        (989001, 990000): 352,
        (815001, 816000): 357,
        (214001, 215000): 293,
        (263001, 264000): 407,
        (513001, 514000): 351,
        (930001, 931000): 370,
        (586001, 587000): 359,
        (814001, 815000): 313,
        (506001, 507000): 382,
        (308001, 309000): 371,
        (634001, 635000): 310,
        (682001, 683000): 305,
        (817001, 818000): 344,
        (834001, 835000): 344,
        (873001, 874000): 326,
        (256001, 257000): 332,
        (957001, 958000): 352,
        (280001, 281000): 345,
        (730001, 731000): 380,
        (449001, 450000): 369,
        (399001, 400000): 299,
        (800001, 801000): 375,
        (621001, 622000): 372,
        (613001, 614000): 310,
        (298001, 299000): 371,
        (484001, 485000): 320,
        (926001, 927000): 370,
        (327001, 328000): 322,
        (472001, 473000): 338,
        (976001, 977000): 383,
        (194001, 195000): 342,
        (41001, 42000): 257,
        (772001, 773000): 393,
        (247001, 248000): 350,
        (701001, 702000): 411,
        (584001, 585000): 372,
        (470001, 471000): 307,
        (757001, 758000): 331,
        (524001, 525000): 364,
        (963001, 964000): 414,
        (203001, 204000): 311,
        (857001, 858000): 357,
        (295001, 296000): 358,
        (545001, 546000): 346,
        (166001, 167000): 352,
        (310001, 311000): 371,
        (891001, 892000): 326,
        (713001, 714000): 380,
        (842001, 843000): 388,
        (427001, 428000): 374,
        (507001, 508000): 364,
        (264001, 265000): 332,
        (261001, 262000): 332,
        (412001, 413000): 343,
        (50001, 51000): 278,
        (135001, 136000): 344,
        (152001, 153000): 308,
        (561001, 562000): 328,
        (82001, 83000): 320,
        (651001, 652000): 336,
        (917001, 918000): 383,
        (483001, 484000): 307,
        (69001, 70000): 299,
        (571001, 572000): 359,
        (31001, 32000): 285,
        (138001, 139000): 313,
        (363001, 364000): 317,
        (283001, 284000): 358,
        (396001, 397000): 330,
        (645001, 646000): 310,
        (248001, 249000): 288,
        (21001, 22000): 269,
        (861001, 862000): 326,
        (376001, 377000): 423,
        (890001, 891000): 339,
        (783001, 784000): 331,
        (422001, 423000): 356,
        (188001, 189000): 316,
        (170001, 171000): 290,
        (104001, 105000): 310,
        (426001, 427000): 356,
        (798001, 799000): 300,
        (794001, 795000): 331,
        (600001, 601000): 372,
        (95001, 96000): 328,
        (786001, 787000): 300,
        (361001, 362000): 348,
        (352001, 353000): 335,
        (189001, 190000): 360,
        (52001, 53000): 340,
        (37001, 38000): 306,
        (660001, 661000): 323,
        (116001, 117000): 305,
        (955001, 956000): 339,
        (259001, 260000): 345,
        (394001, 395000): 405,
        (577001, 578000): 390,
        (300001, 301000): 371,
        (759001, 760000): 362,
        (910001, 911000): 476,
        (553001, 554000): 346,
        (943001, 944000): 352,
        (291001, 292000): 327,
        (473001, 474000): 307,
        (854001, 855000): 419,
        (799001, 800000): 375,
        (232001, 233000): 337,
        (816001, 817000): 344,
        (395001, 396000): 312,
        (359001, 360000): 348,
        (580001, 581000): 266,
        (551001, 552000): 377,
        (581001, 582000): 328,
        (38001, 39000): 244,
        (558001, 559000): 315,
        (830001, 831000): 344,
        (782001, 783000): 331,
        (379001, 380000): 361,
        (417001, 418000): 343,
        (911001, 912000): 383,
        (990001, 991000): 321,
        (696001, 697000): 336,
        (230001, 231000): 443,
        (276001, 277000): 314,
        (98001, 99000): 284,
        (397001, 398000): 330,
        (461001, 462000): 444,
        (733001, 734000): 349,
        (572001, 573000): 372,
        (139001, 140000): 313,
        (722001, 723000): 411,
        (877001, 878000): 401,
        (665001, 666000): 442,
        (190001, 191000): 329,
        (528001, 529000): 377,
        (966001, 967000): 365,
        (619001, 620000): 341,
        (739001, 740000): 424,
        (61001, 62000): 317,
        (206001, 207000): 373,
        (177001, 178000): 334,
        (268001, 269000): 270,
        (901001, 902000): 401,
        (751001, 752000): 331,
        (562001, 563000): 359,
        (83001, 84000): 289,
        (512001, 513000): 333,
        (977001, 978000): 334,
        (1001, 2000): 182,
        (464001, 465000): 338,
        (433001, 434000): 343,
        (249001, 250000): 350,
        (978001, 979000): 383,
        (686001, 687000): 398,
        (852001, 853000): 326,
        (378001, 379000): 330,
        (322001, 323000): 309,
        (671001, 672000): 323,
        (274001, 275000): 332,
        (171001, 172000): 334,
        (132001, 133000): 331,
        (998001, 999000): 396,
        (869001, 870000): 388,
        (812001, 813000): 362,
        (875001, 876000): 432,
        (717001, 718000): 349,
        (316001, 317000): 353,
        (946001, 947000): 383,
        (707001, 708000): 349,
        (117001, 118000): 305,
        (950001, 951000): 414,
        (909001, 910000): 326,
        (962001, 963000): 383,
        (371001, 372000): 348,
        (945001, 946000): 352,
        (593001, 594000): 328,
        (102001, 103000): 310,
        (500001, 501000): 364,
        (502001, 503000): 426,
        (591001, 592000): 403,
        (121001, 122000): 336,
        (75001, 76000): 307,
        (196001, 197000): 298,
        (679001, 680000): 367,
        (867001, 868000): 344,
        (15001, 16000): 271,
        (862001, 863000): 326,
        (860001, 861000): 357,
        (939001, 940000): 507,
        (474001, 475000): 382,
        (477001, 478000): 382,
        (924001, 925000): 370,
        (745001, 746000): 349,
        (664001, 665000): 367,
        (514001, 515000): 364,
        (73001, 74000): 294,
        (813001, 814000): 375,
        (369001, 370000): 361,
        (724001, 725000): 362,
        (96001, 97000): 297,
        (684001, 685000): 336,
        (454001, 455000): 338,
        (4001, 5000): 215,
        (535001, 536000): 302,
        (908001, 909000): 370,
        (698001, 699000): 367,
        (159001, 160000): 352,
        (23001, 24000): 282,
        (669001, 670000): 336,
        (439001, 440000): 356,
        (806001, 807000): 344,
        (804001, 805000): 331,
        (234001, 235000): 381,
        (22001, 23000): 269,
        (985001, 986000): 427,
        (457001, 458000): 338,
        (863001, 864000): 344,
        (849001, 850000): 357,
        (416001, 417000): 387,
        (667001, 668000): 367,
        (731001, 732000): 336,
        (90001, 91000): 315,
        (986001, 987000): 352,
        (403001, 404000): 343,
        (997001, 998000): 440,
        (312001, 313000): 384,
        (811001, 812000): 287,
        (463001, 464000): 325,
        (458001, 459000): 307,
        (260001, 261000): 332,
        (27001, 28000): 259,
        (677001, 678000): 336,
        (120001, 121000): 318,
        (9001, 10000): 260,
        (938001, 939000): 445,
        (241001, 242000): 275,
        (34001, 35000): 311,
        (583001, 584000): 434,
        (742001, 743000): 349,
        (251001, 252000): 319,
        (865001, 866000): 388,
        (844001, 845000): 357,
        (935001, 936000): 414,
        (404001, 405000): 343,
        (974001, 975000): 383,
        (832001, 833000): 388,
        (81001, 82000): 320,
        (329001, 330000): 353,
        (130001, 131000): 331,
        (544001, 545000): 315,
        (871001, 872000): 432,
        (625001, 626000): 354,
        (148001, 149000): 326,
        (187001, 188000): 329,
        (142001, 143000): 375,
        (26001, 27000): 308,
        (489001, 490000): 351,
        (456001, 457000): 356,
        (471001, 472000): 276,
        (220001, 221000): 324,
        (790001, 791000): 375,
        (615001, 616000): 447,
        (589001, 590000): 297,
        (641001, 642000): 372,
        (777001, 778000): 393,
        (277001, 278000): 389,
        (99001, 100000): 328,
        (453001, 454000): 369,
        (58001, 59000): 273,
        (296001, 297000): 327,
        (747001, 748000): 362,
        (239001, 240000): 350,
        (541001, 542000): 364,
        (835001, 836000): 313,
        (858001, 859000): 370,
        (373001, 374000): 348,
        (377001, 378000): 361,
        (229001, 230000): 306,
        (235001, 236000): 306,
        (523001, 524000): 302,
        (93001, 94000): 284,
        (735001, 736000): 424,
        (627001, 628000): 341,
        (719001, 720000): 349,
        (727001, 728000): 318,
        (539001, 540000): 346,
        (623001, 624000): 354,
        (697001, 698000): 318,
        (657001, 658000): 354,
        (620001, 621000): 372,
        (304001, 305000): 340,
        (851001, 852000): 357,
        (324001, 325000): 384,
        (548001, 549000): 333,
        (114001, 115000): 305,
        (181001, 182000): 316,
        (763001, 764000): 375,
        (6001, 7000): 262,
        (455001, 456000): 338,
        (878001, 879000): 326,
        (788001, 789000): 406,
        (576001, 577000): 390,
        (582001, 583000): 328,
        (11001, 12000): 250,
        (131001, 132000): 300,
        (881001, 882000): 326,
        (272001, 273000): 314,
        (932001, 933000): 370,
        (315001, 316000): 340,
        (563001, 564000): 328,
        (565001, 566000): 328,
        (825001, 826000): 344,
        (162001, 163000): 339,
        (879001, 880000): 357,
        (303001, 304000): 340,
        (758001, 759000): 362,
        (7001, 8000): 252,
        (413001, 414000): 374,
        (387001, 388000): 299,
        (823001, 824000): 375,
        (649001, 650000): 385,
        (527001, 528000): 377,
        (964001, 965000): 352,
        (596001, 597000): 372,
        (286001, 287000): 314,
        (410001, 411000): 449,
        (425001, 426000): 356,
        (818001, 819000): 450,
        (981001, 982000): 321,
        (407001, 408000): 268,
        (517001, 518000): 333,
        (787001, 788000): 362,
        (848001, 849000): 313,
        (18001, 19000): 261,
        (222001, 223000): 324,
        (899001, 900000): 295,
        (328001, 329000): 291,
        (176001, 177000): 334,
        (778001, 779000): 437,
        (999001, 1000000): 396,
        (633001, 634000): 354,
        (741001, 742000): 318,
        (674001, 675000): 367,
        (629001, 630000): 279,
        (700001, 701000): 367,
        (221001, 222000): 355,
        (710001, 711000): 380,
        (80001, 81000): 320,
        (79001, 80000): 307,
        (72001, 73000): 263,
        (611001, 612000): 354,
        (127001, 128000): 287,
        (872001, 873000): 326,
        (339001, 340000): 366,
        (429001, 430000): 281,
        (942001, 943000): 339,
        (626001, 627000): 509,
        (53001, 54000): 322,
        (345001, 346000): 441,
        (144001, 145000): 326,
        (105001, 106000): 341,
        (725001, 726000): 305,
        (904001, 905000): 383,
        (805001, 806000): 375,
        (228001, 229000): 355,
        (894001, 895000): 370,
        (192001, 193000): 347,
        (67001, 68000): 312,
        (703001, 704000): 349,
        (779001, 780000): 362,
        (743001, 744000): 318,
        (432001, 433000): 387,
        (706001, 707000): 305,
        (936001, 937000): 383,
        (902001, 903000): 383,
        (208001, 209000): 311,
        (282001, 283000): 314,
        (801001, 802000): 406,
        (692001, 693000): 323,
        (441001, 442000): 294,
        (840001, 841000): 357,
        (605001, 606000): 341,
        (246001, 247000): 332,
        (740001, 741000): 393,
        (845001, 846000): 401,
        (607001, 608000): 341,
        (325001, 326000): 340,
        (490001, 491000): 320,
        (62001, 63000): 286,
        (466001, 467000): 369,
        (89001, 90000): 315,
        (88001, 89000): 333,
        (838001, 839000): 344,
        (161001, 162000): 290,
        (341001, 342000): 335,
        (756001, 757000): 362,
        (723001, 724000): 349,
        (273001, 274000): 358,
        (346001, 347000): 348,
        (996001, 997000): 365,
        (288001, 289000): 389,
        (109001, 110000): 310,
        (219001, 220000): 355,
        (182001, 183000): 334,
        (653001, 654000): 385,
        (71001, 72000): 325,
        (984001, 985000): 352,
        (223001, 224000): 324,
        (850001, 851000): 357,
        (321001, 322000): 353,
        (442001, 443000): 356,
        (279001, 280000): 314,
        (870001, 871000): 326,
        (559001, 560000): 284,
        (533001, 534000): 377,
        (349001, 350000): 366,
        (592001, 593000): 328,
        (255001, 256000): 288,
        (467001, 468000): 413,
        (708001, 709000): 349,
        (205001, 206000): 355,
        (307001, 308000): 309,
        (546001, 547000): 452,
        (284001, 285000): 358,
        (685001, 686000): 354,
        (542001, 543000): 346,
        (78001, 79000): 338,
        (912001, 913000): 401,
        (330001, 331000): 322,
        (137001, 138000): 331,
        (555001, 556000): 390,
        (824001, 825000): 326,
        (922001, 923000): 445,
        (3001, 4000): 238,
        (887001, 888000): 401,
        (32001, 33000): 267,
        (178001, 179000): 347,
        (702001, 703000): 380,
        (672001, 673000): 367,
        (25001, 26000): 264,
        (153001, 154000): 308,
        (819001, 820000): 357,
        (709001, 710000): 336,
        (716001, 717000): 380,
        (118001, 119000): 336,
        (557001, 558000): 315,
        (498001, 499000): 351,
        (242001, 243000): 319,
        (560001, 561000): 359,
        (331001, 332000): 291,
        (84001, 85000): 320,
        (233001, 234000): 368,
        (30001, 31000): 272,
        (344001, 345000): 304,
        (608001, 609000): 403,
        (151001, 152000): 295,
        (5001, 6000): 236,
        (622001, 623000): 310,
        (36001, 37000): 249,
        (431001, 432000): 343,
        (603001, 604000): 297,
        (785001, 786000): 375,
        (828001, 829000): 313,
        (959001, 960000): 352,
        (906001, 907000): 445,
        (204001, 205000): 311,
        (568001, 569000): 359,
        (186001, 187000): 347,
        (631001, 632000): 310,
        (428001, 429000): 294,
        (462001, 463000): 369,
        (450001, 451000): 387,
        (225001, 226000): 368,
        (207001, 208000): 311,
        (57001, 58000): 304,
        (683001, 684000): 380,
        (254001, 255000): 363,
        (837001, 838000): 525,
        (451001, 452000): 307,
        (393001, 394000): 361,
        (147001, 148000): 295,
        (68001, 69000): 312,
        (753001, 754000): 424,
        (46001, 47000): 283,
        (482001, 483000): 351,
        (353001, 354000): 304,
        (164001, 165000): 334,
        (846001, 847000): 313,
        (16001, 17000): 266,
        (56001, 57000): 322,
        (773001, 774000): 362,
        (340001, 341000): 304,
        (174001, 175000): 334,
        (704001, 705000): 504,
        (973001, 974000): 383,
        (579001, 580000): 359,
        (770001, 771000): 349,
        (405001, 406000): 405,
        (993001, 994000): 290,
        (754001, 755000): 318,
        (150001, 151000): 370,
        (948001, 949000): 383,
        (59001, 60000): 304,
        (380001, 381000): 379,
        (543001, 544000): 359,
        (289001, 290000): 345,
        (689001, 690000): 336,
        (567001, 568000): 359,
        (612001, 613000): 310,
        (112001, 113000): 323,
        (385001, 386000): 348,
        (859001, 860000): 313,
        (356001, 357000): 348,
        (822001, 823000): 344,
        (690001, 691000): 336,
        (133001, 134000): 313,
        (855001, 856000): 388,
        (42001, 43000): 288,
        (499001, 500000): 395,
        (469001, 470000): 338,
        (797001, 798000): 344,
        (644001, 645000): 279,
        (392001, 393000): 299,
        (486001, 487000): 382,
        (771001, 772000): 344,
        (497001, 498000): 289,
        (602001, 603000): 297,
        (661001, 662000): 323,
        (468001, 469000): 382,
        (250001, 251000): 332,
        (694001, 695000): 367,
        (375001, 376000): 361,
        (587001, 588000): 328,
        (737001, 738000): 380,
        (335001, 336000): 322,
        (803001, 804000): 300,
        (348001, 349000): 335,
        (827001, 828000): 419,
        (614001, 615000): 310,
        (884001, 885000): 295,
        (728001, 729000): 362,
        (47001, 48000): 314,
        (693001, 694000): 367,
        (616001, 617000): 372,
        (639001, 640000): 354,
        (218001, 219000): 324,
        (521001, 522000): 364,
        (155001, 156000): 308,
        (638001, 639000): 354,
        (167001, 168000): 321,
        (355001, 356000): 379,
        (226001, 227000): 368,
        (821001, 822000): 375,
        (54001, 55000): 260,
        (918001, 919000): 339,
        (128001, 129000): 331,
        (530001, 531000): 346,
        (526001, 527000): 408,
        (925001, 926000): 339,
        (648001, 649000): 323,
        (292001, 293000): 340,
        (285001, 286000): 376,
        (444001, 445000): 325,
        (898001, 899000): 370,
        (655001, 656000): 385,
        (995001, 996000): 365,
        (775001, 776000): 437,
        (446001, 447000): 325,
        (424001, 425000): 356,
        (991001, 992000): 321,
        (390001, 391000): 361,
        (714001, 715000): 305,
        (224001, 225000): 324,
        (125001, 126000): 318,
        (795001, 796000): 331,
        (504001, 505000): 364,
        (421001, 422000): 325,
        (847001, 848000): 419,
        (367001, 368000): 286,
        (86001, 87000): 302,
        (157001, 158000): 339,
        (643001, 644000): 279,
        (972001, 973000): 321,
        (302001, 303000): 340,
        (746001, 747000): 287,
        (123001, 124000): 331,
        (764001, 765000): 362,
        (415001, 416000): 312,
        (14001, 15000): 271,
        (820001, 821000): 450,
        (294001, 295000): 296,
        (853001, 854000): 357,
        (864001, 865000): 326,
        (509001, 510000): 364,
        (110001, 111000): 292,
        (532001, 533000): 377,
        (212001, 213000): 355,
        (729001, 730000): 318,
        (784001, 785000): 313,
        (688001, 689000): 292,
        (266001, 267000): 314,
        (659001, 660000): 354,
        (928001, 929000): 383,
        (101001, 102000): 310,
        (980001, 981000): 427,
        (610001, 611000): 341,
        (501001, 502000): 320,
        (670001, 671000): 323,
        (940001, 941000): 383,
        (781001, 782000): 362,
        (168001, 169000): 321,
        (332001, 333000): 353,
        (297001, 298000): 327,
        (829001, 830000): 375,
        (675001, 676000): 385,
        (968001, 969000): 321,
        (195001, 196000): 360,
        (94001, 95000): 315,
        (927001, 928000): 476,
        (913001, 914000): 357,
        (895001, 896000): 326,
        (520001, 521000): 333,
        (197001, 198000): 285,
        (398001, 399000): 374,
        (915001, 916000): 339,
        (718001, 719000): 336,
        (334001, 335000): 322,
        (929001, 930000): 339,
        (960001, 961000): 414,
        (28001, 29000): 259,
        (448001, 449000): 369,
        (493001, 494000): 395,
        (338001, 339000): 335,
        (585001, 586000): 359,
        (160001, 161000): 370,
        (77001, 78000): 351,
        (618001, 619000): 310,
        (720001, 721000): 411,
        (179001, 180000): 303,
        (921001, 922000): 339,
        (386001, 387000): 330,
        (44001, 45000): 270,
        (780001, 781000): 344,
        (443001, 444000): 338,
        (419001, 420000): 281,
        (97001, 98000): 297,
        (630001, 631000): 341,
        (975001, 976000): 339,
        (384001, 385000): 330,
        (333001, 334000): 335,
        (503001, 504000): 320,
        (211001, 212000): 355,
        (115001, 116000): 349,
        (570001, 571000): 377,
        (460001, 461000): 307,
        (914001, 915000): 370,
        (594001, 595000): 328,
        (481001, 482000): 382,
        (609001, 610000): 359,
        (270001, 271000): 407,
        (45001, 46000): 314,
        (808001, 809000): 331,
        (575001, 576000): 346,
        (876001, 877000): 370,
        (238001, 239000): 306,
        (937001, 938000): 352,
        (903001, 904000): 295,
        (408001, 409000): 312,
        (414001, 415000): 374,
        (492001, 493000): 426,
        (19001, 20000): 274,
        (290001, 291000): 327,
        (149001, 150000): 326,
        (809001, 810000): 344,
        (536001, 537000): 333,
        (402001, 403000): 268,
        (252001, 253000): 319,
        (760001, 761000): 380,
        (905001, 906000): 295,
        (691001, 692000): 442,
        (988001, 989000): 321,
        (193001, 194000): 329,
        (146001, 147000): 357,
        (982001, 983000): 383,
        (85001, 86000): 302,
        (401001, 402000): 387,
        (364001, 365000): 317,
        (642001, 643000): 416,
        (257001, 258000): 345,
        (420001, 421000): 343,
        (807001, 808000): 406,
        (663001, 664000): 292,
        (370001, 371000): 392,
        (374001, 375000): 330,
        (217001, 218000): 324,
        (66001, 67000): 268,
        (540001, 541000): 408,
        (106001, 107000): 354,
        (64001, 65000): 299,
        (956001, 957000): 352,
        (465001, 466000): 369,
        (240001, 241000): 368,
    }
    return max_cycle_length_cache


# ------------
# collatz_read
# ------------


def collatz_read(string: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    split_string = string.split()
    return [int(split_string[0]), int(split_string[1])]


# ------------
# cycle_length
# ------------


def cycle_length(num: int) -> int:
    """
    returns the cycle length of one int using the Collatz algorithm.
    """
    num_copy = num
    cycle_count = 1
    while num_copy != 1:
        if num_copy % 2 == 1:
            # if odd, we will do 3n + 1 and then divide by 2
            num_copy = num_copy + (num_copy >> 1) + 1 # this combines it in one step and uses bit shifts for speed
            cycle_count += 1
        else:
            num_copy = num_copy >> 1
        cycle_count += 1
    return cycle_count


# ------------
# collatz_eval
# ------------


def collatz_eval(range_one: int, range_two: int) -> int:
    """
    range_one the beginning of the givent range, inclusive
    range_two the end       of the range, inclusive
    return the max cycle length of the range [range_one, range_two]
    """
    # <your code>
    max_cycle_length_cache = create_cache() # downloads the meta cache
    max_cycle_length = -1
    min_range = min(
        range_one, range_two
    )  # gets the beginning of the range, must be the smaller number of the pair
    max_range = max(range_one, range_two) # gets the end of the range, must be the larger number of the pair
    mid_range = (max_range >> 1) + 1
    current_num = mid_range if min_range < mid_range else min_range # an optimization that sees if we can reduce the range
    while current_num != max_range + 1:
        if (current_num % 1000) == 1 and (max_range - current_num) >= 1000:
            # we are at the start of a cached range
            max_cycle_length = max(
                max_cycle_length,
                max_cycle_length_cache[(current_num, current_num + 999)],
            ) # lookup
            current_num = current_num + 1000 # we can now check something 1000 away
        else:
            current_num_remainder = current_num % 1000
            stop_num = (
                current_num - current_num_remainder + 1000
                if (max_range - current_num) > 1000
                else max_range
            ) # we either move to the next beginning of a range if we have enough left to use look up, otherwise we just finish out
            for num in range(current_num, stop_num + 1):
                max_cycle_length = max(max_cycle_length, cycle_length(num)) # manually compares each cycle length in the range
            current_num = stop_num + 1 # puts at the beginning of the next range, which is either the end of the range or beginning of the next multiple of 1000
    return max_cycle_length


# -------------
# collatz_print
# -------------


def collatz_print(
    write_to: IO[str], range_one: int, range_two: int, max_cycle_length: int
) -> None:
    """
    print three ints
    write_to a writer
    range_one the beginning of the range, inclusive
    range_two the end       of the range, inclusive
    max_cycle_length the max cycle length
    """
    write_to.write(
        str(range_one) + " " + str(range_two) + " " + str(max_cycle_length) + "\n"
    )


# -------------
# collatz_solve
# -------------


def collatz_solve(read_from: IO[str], write_to: IO[str]) -> None:
    """
    read_from a reader
    write_t0 a writer
    """
    for string in read_from:
        range_one, range_two = collatz_read(string)
        max_cycle_length = collatz_eval(range_one, range_two)
        collatz_print(write_to, range_one, range_two, max_cycle_length)
