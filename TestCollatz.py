#!/usr/bin/env python3
"""
Used to test Collatz.py functions
"""
# --------------
# TestCollatz.py
# --------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import (
    collatz_read,
    collatz_eval,
    collatz_print,
    collatz_solve,
    cycle_length,
    create_cache,
)

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    """
    unit tests for the collatz methods from collatz.py
    """

    # ----
    # read
    # ----

    def test_read(self):
        """
        tests read of values
        """
        test_string = "1 10\n"
        range_one, range_two = collatz_read(test_string)
        self.assertEqual(range_one, 1)
        self.assertEqual(range_two, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        """
        test max cycle length on [1, 10]
        """
        max_cycle_length = collatz_eval(1, 10)
        self.assertEqual(max_cycle_length, 20)

    def test_eval_2(self):
        """
        test max cycle length on [100, 200]
        """
        max_cycle_length = collatz_eval(100, 200)
        self.assertEqual(max_cycle_length, 125)

    def test_eval_3(self):
        """
        test max cycle length on [201, 210]
        """
        max_cycle_length = collatz_eval(201, 210)
        self.assertEqual(max_cycle_length, 89)

    def test_eval_4(self):
        """
        test max cycle length on [900, 1000]
        """
        max_cycle_length = collatz_eval(900, 1000)
        self.assertEqual(max_cycle_length, 174)

    def test_eval_5(self):
        """
        test max cycle length on [230018, 640305]
        """
        max_cycle_length = collatz_eval(230018, 640305)
        self.assertEqual(max_cycle_length, 509)

    # ----
    # cycle_length
    # ----

    def test_cycle_length_1(self):
        """
        test cycle length on 22
        """
        cycle_length_value = cycle_length(22)
        self.assertEqual(cycle_length_value, 16)

    def test_cycle_length_2(self):
        """
        test cycle length on 33
        """
        cycle_length_value = cycle_length(33)
        self.assertEqual(cycle_length_value, 27)

    def test_cycle_length_3(self):
        """
        test cycle length on 25
        """
        cycle_length_value = cycle_length(25)
        self.assertEqual(cycle_length_value, 24)

    def test_cycle_length_4(self):
        """
        test cycle length on 1
        """
        cycle_length_value = cycle_length(1)
        self.assertEqual(cycle_length_value, 1)

    def test_cycle_length_5(self):
        """
        test cycle length on 100
        """
        cycle_length_value = cycle_length(100)
        self.assertEqual(cycle_length_value, 26)

    # -----
    # create_cache
    # -----

    def test_create_cache(self):
        """
        tests to see if meta cache is being created
        """
        max_cycle_length_cache = create_cache()
        self.assertTrue(len(max_cycle_length_cache) != 0)

    # -----
    # print
    # -----

    def test_print(self):
        """
        test printing values
        """
        write_to = StringIO()
        collatz_print(write_to, 1, 10, 20)
        self.assertEqual(write_to.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        """
        test solving method
        """
        read_from = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        write_to = StringIO()
        collatz_solve(read_from, write_to)
        self.assertEqual(
            write_to.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
